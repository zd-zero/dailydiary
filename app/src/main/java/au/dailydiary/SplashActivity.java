package au.dailydiary;

import static au.dailydiary.DailyDiaryConstants.KEY_APP_PREF;
import static au.dailydiary.DailyDiaryConstants.KEY_PASS_CODE;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import au.dailydiary.login.LoginActivity;
import au.dailydiary.login.LoginActivity.Action;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash );

        SharedPreferences prefs = this.getSharedPreferences( KEY_APP_PREF, Context.MODE_PRIVATE );
        final String passCode = prefs.getString( KEY_PASS_CODE, "" );

        int SPLASH_TIME_OUT = 1500;
        new Handler().postDelayed( () -> {
            Action action = Action.VERIFY;
            if ( passCode == null || passCode.equals( "" ) )
                action = Action.NEW;

            startActivity( LoginActivity.getStartIntent( this, action ) );
            finish();
        }, SPLASH_TIME_OUT );
    }
}
