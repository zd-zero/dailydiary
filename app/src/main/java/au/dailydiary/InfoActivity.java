package au.dailydiary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class InfoActivity extends AppCompatActivity {

    public static Intent getStartIntent( Context context ) {
        return new Intent( context, InfoActivity.class );
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_info );
    }

    public void onClickHome(View view) {
        finish();
    }
}
