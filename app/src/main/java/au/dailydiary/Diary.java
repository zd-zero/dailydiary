package au.dailydiary;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import android.os.Parcel;
import android.os.Parcelable;

public class Diary implements Parcelable {

    private UUID uuid;

    private String content;

    private Date date;

    public Diary( Parcel source ) {
        this.uuid = ( UUID ) source.readValue( ClassLoader.getSystemClassLoader() );
        this.content = source.readString();
        this.date = ( Date ) source.readValue( ClassLoader.getSystemClassLoader() );
    }

    public Diary( String diary ) {
        this( diary, new Date() );
    }

    public Diary( String diary, Date date ) {
        this.uuid = UUID.randomUUID();
        this.content = diary;
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeValue( uuid );
        dest.writeString( content );
        dest.writeValue( date );
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent( String content ) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate( Date date ) {
        this.date = date;
    }

    public String getStringDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "E, MMMM dd, yyyy", Locale.ENGLISH );
        return simpleDateFormat.format( date );
    }

    public String getStringTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "h:mm a", Locale.ENGLISH );
        return simpleDateFormat.format( date );
    }

    public static final Parcelable.Creator<Diary> CREATOR = new Creator<Diary>() {
        @Override
        public Diary createFromParcel( Parcel source ) {
            return new Diary( source );
        }

        @Override
        public Diary[] newArray( int size ) {
            return new Diary[size];
        }
    };
}
