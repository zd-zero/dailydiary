package au.dailydiary;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DiaryActivity extends AppCompatActivity {

    private static final String EXTRA_REQUEST_ACTION = "EXTRA_REQUEST_ACTION";

    public static final String EXTRA_DIARY = "EXTRA_DIARY";

    private Diary diary;

    EditText diaryEditText;

    public static Intent getStartIntent( Context context, int requestAction, Diary diary ) {
        Intent intent = new Intent( context, DiaryActivity.class );
        intent.putExtra( EXTRA_REQUEST_ACTION, requestAction );
        intent.putExtra( EXTRA_DIARY, diary );
        return intent;
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_diary );

        this.diary = getIntent().getParcelableExtra( EXTRA_DIARY );
        diaryEditText = findViewById( R.id.et_diary );

        Date date = diary.getDate();
        Calendar calendar = Calendar.getInstance( Locale.getDefault() );
        calendar.setTime( date );
        int dayOfMonth = calendar.get( Calendar.DAY_OF_MONTH );
        System.out.println( "Day of month: " + dayOfMonth );

        TextView monthTextView = findViewById( R.id.tv_month );
        monthTextView.setText( calendar.getDisplayName( Calendar.MONTH, Calendar.LONG, Locale.getDefault() ) );

        TextView dateTextView = findViewById( R.id.tv_date );
        String day = dayOfMonth + "";
        dateTextView.setText( day );

        diaryEditText.setText( diary.getContent() );
    }

    public void onClickBack( View view ) {
        finish();
    }

    public void onClickSave( View view ) {
        String diaryText = diaryEditText.getText().toString();
        this.diary.setContent( diaryText );

        if ( diaryText.isEmpty() )
            setResult( RESULT_CANCELED );
        else {
            Intent intent = new Intent();
            intent.putExtra( EXTRA_DIARY, this.diary );
            setResult( RESULT_OK, intent );
        }
        finish();
    }
}
