package au.dailydiary.list;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import au.dailydiary.DailyDiaryConstants;
import au.dailydiary.Diary;
import au.dailydiary.DiaryActivity;
import au.dailydiary.R;
import es.dmoral.toasty.Toasty;

public class DiaryListActivity extends AppCompatActivity {

    public static final int REQUEST_DIARY_ADD = 0;

    public static final int REQUEST_DIARY_EDIT = 1;

    private ViewSwitcher viewSwitcher;

    private DiaryArrayAdapter diaryArrayAdapter;

    private List<Diary> diaries;

    private Calendar currentDate;

    private SharedPreferences prefs;

    public static Intent getStartIntent( Context context ) {
        return new Intent( context, DiaryListActivity.class );
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_diary_list );

        this.diaries = new ArrayList<>();
        this.currentDate = Calendar.getInstance( Locale.getDefault() );
        this.currentDate.setTime( new Date() );
        this.diaryArrayAdapter = new DiaryArrayAdapter( diaries );
        this.prefs = this.getSharedPreferences(
                DailyDiaryConstants.KEY_APP_PREF, Context.MODE_PRIVATE );

        CalendarView calendarView = findViewById( R.id.calendarView );
        this.viewSwitcher = findViewById( R.id.view_switcher );

        RecyclerView diaryRecyclerView = findViewById( R.id.recycler_view_diary );
        diaryRecyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager( getApplicationContext() );
        diaryRecyclerView.setLayoutManager( linearLayoutManager );
        diaryRecyclerView.setItemAnimator( new DefaultItemAnimator() );
        diaryRecyclerView.addItemDecoration(
                new DividerItemDecoration( getApplicationContext(), LinearLayoutManager.VERTICAL ) );
        diaryRecyclerView.setAdapter( diaryArrayAdapter );

        calendarView.setOnDateChangeListener( ( view, year, month, dayOfMonth ) -> {
            currentDate.set( year, month, dayOfMonth );
            updateDiaryList();
        } );

        retrieveData();
        updateDiaryList();
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, @Nullable Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );

        if ( data == null )
            return;

        Diary diary = data.getParcelableExtra( DiaryActivity.EXTRA_DIARY );

        if ( requestCode == REQUEST_DIARY_ADD ) {
            diaries.add( diary );
            updateDiaryList();

            diaryArrayAdapter.selectDiary( diary.getUuid() );
            Toasty.success( this, getResources().getString( R.string.msg_diary_added_successfully ), Toast.LENGTH_SHORT,
                    true ).show();
        } else if ( requestCode == REQUEST_DIARY_EDIT ) {
            for ( Diary d : diaries ) {
                if ( diary.getUuid().equals( d.getUuid() ) ) {
                    d.setContent( diary.getContent() );
                    d.setDate( diary.getDate() );
                    updateDiaryList();

                    Toasty.success( this, getResources().getString( R.string.msg_diary_updated_successfully ),
                            Toast.LENGTH_SHORT, true ).show();
                    break;
                }
            }
        }

        persistData();
    }

    public void onClickBack( View view ) {
        finish();
    }

    public void onClickAdd( View view ) {
        int requestAction = REQUEST_DIARY_ADD;

        startActivityForResult( DiaryActivity.getStartIntent( this, requestAction, new Diary( "", getSelectedDate() ) ),
                requestAction );
    }

    public void onClickEdit( View view ) {
        Diary selectedDiary = diaryArrayAdapter.getSelectedDiary();
        if ( diaryArrayAdapter.getItemCount() == 0 || selectedDiary == null ) {
            displaySelectionError();
            return;
        }

        Diary diary = getDiarySelection( selectedDiary.getUuid() );

        if ( diary != null ) {
            int requestAction = REQUEST_DIARY_EDIT;
            startActivityForResult( DiaryActivity.getStartIntent( this, requestAction, diary ), requestAction );
        }
    }

    public void onClickDelete( View view ) {
        Diary selectedDiary = diaryArrayAdapter.getSelectedDiary();
        if ( diaryArrayAdapter.getItemCount() == 0 || selectedDiary == null ) {
            displaySelectionError();
            return;
        }

        Builder dialogBuilder = new Builder( this )
                .setMessage( "Are you sure you want to delete the selected diary?" )
                .setCancelable( true );

        dialogBuilder.setPositiveButton( "Yes", ( dialog, which ) -> {
            Diary diaryToRemove = getDiarySelection( selectedDiary.getUuid() );

            if ( diaryToRemove != null ) {
                diaries.remove( diaryToRemove );
                updateDiaryList();

                if ( diaryArrayAdapter.getItemCount() != 0 )
                    diaryArrayAdapter.setSelectedPosition( 0 );

                Toasty.success( this, getResources().getString( R.string.msg_diary_deleted_successfully ),
                        Toast.LENGTH_SHORT, true ).show();
            }

            persistData();
            dialog.cancel();
        } );

        dialogBuilder.setNegativeButton( "No", ( dialog, which ) -> dialog.dismiss() );

        dialogBuilder.show();
    }

    private void updateDiaryList() {
        syncDiaryList( getSelectedDate() );
    }

    private Date getSelectedDate() {
        return currentDate.getTime();
    }

    private Diary getDiarySelection( UUID uuid ) {
        Diary diary = null;
        for ( Diary d : diaries ) {
            if ( d.getUuid().equals( uuid ) )
                diary = d;
        }

        return diary;
    }

    private void syncDiaryList( Date date ) {
        List<Diary> currentDiaries = new ArrayList<>();

        for ( Diary diary : diaries ) {
            if ( isSameDay( date, diary.getDate() ) )
                currentDiaries.add( diary );
        }

        diaryArrayAdapter.updateDiaries( currentDiaries );

        if ( currentDiaries.size() == 0 && viewSwitcher.getNextView().getId() == R.id.tv_empty_list ) {
            viewSwitcher.showNext();
        } else if ( currentDiaries.size() > 0 && viewSwitcher.getNextView().getId() == R.id.recycler_view_diary ) {
            viewSwitcher.showNext();
        }
    }

    private boolean isSameDay( Date d1, Date d2 ) {
        Calendar cal1 = Calendar.getInstance( Locale.getDefault() );
        cal1.setTime( d1 );

        Calendar cal2 = Calendar.getInstance( Locale.getDefault() );
        cal2.setTime( d2 );

        return cal1.get( Calendar.YEAR ) == cal2.get( Calendar.YEAR ) && cal1.get( Calendar.MONTH ) == cal2.get(
                Calendar.MONTH ) && cal1.get( Calendar.DAY_OF_MONTH ) == cal2.get( Calendar.DAY_OF_MONTH );
    }

    private void displaySelectionError() {
        new Builder( this ).setMessage( "No diary selected." )
                .setPositiveButton( "Ok", ( dialog, which ) -> dialog.dismiss() )
                .show();
    }

    private void retrieveData() {
        String diariesData = prefs.getString( DailyDiaryConstants.KEY_DIARIES, "" );
        if ( diariesData == null || diariesData.isEmpty() )
            return;

        Gson gson = new Gson();
        this.diaries = gson.fromJson( diariesData, new TypeToken<List<Diary>>() {}.getType() );

        System.out.println( "size: " + diaries.size() );
    }

    private void persistData() {
        Gson gson = new Gson();
        String diariesData = gson.toJson( diaries );

        Editor prefsEditor = prefs.edit();
        prefsEditor.putString( DailyDiaryConstants.KEY_DIARIES, diariesData );
        prefsEditor.apply();
    }
}
