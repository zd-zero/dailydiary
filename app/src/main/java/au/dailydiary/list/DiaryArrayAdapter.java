package au.dailydiary.list;

import java.util.List;
import java.util.UUID;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import au.dailydiary.Diary;
import au.dailydiary.R;

public class DiaryArrayAdapter extends RecyclerView.Adapter<DiaryArrayAdapter.ViewHolder> {

    private List<Diary> diaries;

    private int selectedPosition;

    DiaryArrayAdapter( List<Diary> diaries ) {
        this.diaries = diaries;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( @NonNull ViewGroup viewGroup, int i ) {
        TextView diaryTextView = ( TextView ) LayoutInflater.from( viewGroup.getContext() ).inflate(
                R.layout.item_diary, viewGroup, false );
        return new ViewHolder( diaryTextView );
    }

    @Override
    public void onBindViewHolder( @NonNull ViewHolder viewHolder, int i ) {
        if ( selectedPosition == i ) {
            viewHolder.itemView.setBackgroundColor( Color.parseColor( "#EB4545" ) );
            viewHolder.diaryTextView.setTextColor( Color.WHITE );
        } else {
            viewHolder.itemView.setBackgroundColor( Color.TRANSPARENT );
            viewHolder.diaryTextView.setTextColor( Color.BLACK );
        }

        Diary diary = diaries.get( i );

        viewHolder.itemView.setOnClickListener( view -> {
            notifyItemChanged( selectedPosition );
            selectedPosition = viewHolder.getAdapterPosition();
            notifyItemChanged( selectedPosition );
        } );

        viewHolder.diaryTextView.setText( diary.getContent() );
    }

    @Override
    public int getItemCount() {
        return diaries.size();
    }

    @Nullable
    public Diary getSelectedDiary() {
        int size = this.diaries.size();
        if ( size == 0 || selectedPosition < 0 || selectedPosition > this.diaries.size() )
            return null;

        return this.diaries.get( selectedPosition );
    }

    public void updateDiaries( List<Diary> diaries ) {
        this.diaries = diaries;
        notifyDataSetChanged();
    }

    public void selectDiary( UUID uuid ) {
        int index = -1;
        for ( Diary d : diaries ) {
            if ( d.getUuid().equals( uuid ) ) {
                index = diaries.indexOf( d );
            }
        }

        if ( index != -1 ) {
            setSelectedPosition( index );
            notifyDataSetChanged();
        }
    }

    public void setSelectedPosition( int position ) {
        this.selectedPosition = position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView diaryTextView;

        ViewHolder( @NonNull View itemView ) {
            super( itemView );
            this.diaryTextView = ( TextView ) itemView;
        }
    }
}
