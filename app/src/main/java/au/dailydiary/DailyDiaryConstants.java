package au.dailydiary;

public interface DailyDiaryConstants {

    String KEY_APP_PREF = "au.dailydiary.KEY_APP_PREF";

    String KEY_PASS_CODE = "au.dailydiary.KEY_PASS_CODE";

    String KEY_DIARIES = "au.dailydiary.KEY_DIARIES";
}
