package au.dailydiary.login;

import static au.dailydiary.DailyDiaryConstants.KEY_APP_PREF;
import static au.dailydiary.DailyDiaryConstants.KEY_PASS_CODE;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import au.dailydiary.MainMenuActivity;
import au.dailydiary.R;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {

    private static final String EXTRA_LOGIN_ACTION = "EXTRA_LOGIN_ACTION";

    TextView passCodeTextView;

    private String passCode;

    private Action action;

    private StringBuilder passCodeInput;

    private SharedPreferences prefs;

    public static Intent getStartIntent( Context context, Action action ) {
        Intent intent = new Intent( context, LoginActivity.class );
        intent.putExtra( EXTRA_LOGIN_ACTION, action );
        return intent;
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        prefs = this.getSharedPreferences( KEY_APP_PREF, Context.MODE_PRIVATE );
        passCode = prefs.getString( KEY_PASS_CODE, "" );

        action = ( Action ) getIntent().getSerializableExtra( EXTRA_LOGIN_ACTION );
        TextView passCodeTextView = findViewById( R.id.tv_passcode_title );
        switch ( action ) {
            case NEW:
                passCodeTextView.setText( getResources().getString( R.string.msg_enter_new_passcode ) );
                break;
            case VERIFY:
                passCodeTextView.setText( getResources().getString( R.string.msg_enter_your_passcode ) );
                break;
        }

        this.passCodeTextView = findViewById( R.id.tv_passcode );
        passCodeInput = new StringBuilder();

        Set<Keypad> keypads = new HashSet<>();
        initKeypads( keypads );
    }

    public void onClickBackspace( View view ) {
        if ( passCodeInput.length() == 0 )
            return;

        passCodeInput.setLength( passCodeInput.length() - 1 );
        updatePassCodeTextView();
    }

    public void onClickLogin( View view ) {
        if ( passCodeInput.toString().length() == 0 ) {
            Toasty.error( this, getResources().getString( R.string.msg_please_enter_passcode ), Toast.LENGTH_SHORT,
                    true ).show();
            return;
        }

        switch ( action ) {
            case NEW:
                Editor prefsEditor = prefs.edit();
                prefsEditor.putString( KEY_PASS_CODE, passCodeInput.toString() );
                prefsEditor.apply();

                startActivity( MainMenuActivity.getStartIntent( this ) );
                break;
            case VERIFY:
                if ( passCodeInput.toString().equals( passCode ) )
                    startActivity( MainMenuActivity.getStartIntent( this ) );
                else
                    Toasty.error( this, getResources().getString( R.string.msg_invalid_passcode ), Toast.LENGTH_SHORT,
                            true ).show();
                break;
        }
    }

    private void initKeypads( Set<Keypad> keypads ) {
        keypads.add( new Keypad( 1, findViewById( R.id.tv_one ) ) );
        keypads.add( new Keypad( 2, findViewById( R.id.tv_two ) ) );
        keypads.add( new Keypad( 3, findViewById( R.id.tv_three ) ) );
        keypads.add( new Keypad( 4, findViewById( R.id.tv_four ) ) );
        keypads.add( new Keypad( 5, findViewById( R.id.tv_five ) ) );
        keypads.add( new Keypad( 6, findViewById( R.id.tv_six ) ) );
        keypads.add( new Keypad( 7, findViewById( R.id.tv_seven ) ) );
        keypads.add( new Keypad( 8, findViewById( R.id.tv_eight ) ) );
        keypads.add( new Keypad( 9, findViewById( R.id.tv_nine ) ) );
        keypads.add( new Keypad( 0, findViewById( R.id.tv_zero ) ) );

        for ( Keypad keypad : keypads ) {
            keypad.getTextView().setOnClickListener( v -> {
                passCodeInput.append( keypad.getValue() );
                updatePassCodeTextView();
            } );
        }
    }

    private void updatePassCodeTextView() {
        StringBuilder displayPassCode = new StringBuilder();
        for ( int i = 0; i < passCodeInput.length(); i++ )
            displayPassCode.append( "* " );

        passCodeTextView.setText( displayPassCode.toString() );
    }

    public enum Action {
        NEW, VERIFY
    }
}
