package au.dailydiary.login;

import android.widget.TextView;

public class Keypad {

    private int value;

    private TextView textView;

    public Keypad( int value, TextView textView ) {
        this.value = value;
        this.textView = textView;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView( TextView textView ) {
        this.textView = textView;
    }

    public int getValue() {
        return value;
    }

    public void setValue( int value ) {
        this.value = value;
    }
}
