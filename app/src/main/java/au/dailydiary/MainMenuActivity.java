package au.dailydiary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import au.dailydiary.list.DiaryListActivity;

public class MainMenuActivity extends AppCompatActivity {

    public static Intent getStartIntent( Context context ) {
        return new Intent( context, MainMenuActivity.class );
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main_menu );
    }

    public void onClickOpen( View view ) {
        startActivity( DiaryListActivity.getStartIntent( this ) );
    }

    public void onClickSettings( View view ) {
        startActivity( SettingsActivity.getStartIntent( this ) );
    }

    public void onClickInfo( View view ) {
        startActivity( InfoActivity.getStartIntent( this ) );
    }

    public void onClickHelp( View view ) {
        startActivity( HelpActivity.getStartIntent( this ) );
    }

    public void onClickShare( View view ) {
        Intent intent = new Intent( Intent.ACTION_SEND );
        intent.setType( "text/plain" );
        intent.putExtra( Intent.EXTRA_SUBJECT, getResources().getString( R.string.title_share ) );
        intent.putExtra( Intent.EXTRA_TEXT, getResources().getString( R.string.msg_share_content ) );
        startActivity( Intent.createChooser( intent, getResources().getString( R.string.msg_share_app ) ) );
    }
}
